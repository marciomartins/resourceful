program rc;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.IniFiles,
  FTPClient in 'FTPClient.pas';

var
  Lib: string;
  Dest: string;
  ServerHost: string;
  Port: Word;
  FTPClient: TFTPClient;
  fz: string;
  dv: string;
  fv: string;
  ln: string;
  lv: string;
  d: string;

procedure HelpCmd;
begin
  Writeln('-------------------------');
  Writeln('HELP - Recourceful Client');
  Writeln('-------------------------');
  Writeln('');
  Writeln('Sample use to send:');
  Writeln('rc /s /fz DevExpress.zip /dv D5 /fv C:\Projetos\Delphi\DevExp\Version.txt');
  Writeln('or');
  Writeln('rc -s -fz DevExpress.zip -dv D5 -fv C:\Projetos\Delphi\DevExp\Version.txt');
  Writeln('or');
  Writeln('rc -s -fz:DevExpress.zip -dv:D5 -fv:C:\Projetos\Delphi\DevExp\Version.txt');
  Writeln('');
  Writeln('Sample use to receive:');
  Writeln('rc /r /ln DevExpress /dv D5 /lv 2.3 /d C:\Projetos\Lib\D5\DevExp');
  Writeln('or');
  Writeln('rc -r -ln DevExpress -dv D5 -lv 2.3 -d C:\Projetos\Lib\D5\DevExp');
  Writeln('or');
  Writeln('rc -r -ln:DevExpress -dv:D5 -lv:2.3 -d:C:\Projetos\Lib\D5\DevExp');
  Readln;
  Abort;
end;

procedure ReadConfig;
var
  Ini: TMemIniFile;
  Path: string;
  ConfigFile: string;
begin
  Path := ExtractFilePath(ParamStr(0));
  ConfigFile := Path + 'Config.ini';
  if not FileExists(ConfigFile) then
  begin
    Writeln('File not found: ' + ConfigFile);
    Readln;
    Abort;
  end;

  Ini := TMemIniFile.Create(Path+'Config.ini');
  try
    ServerHost := Ini.ReadString('Server', 'Host', '');
    Port := Ini.ReadInteger('Server', 'Port', 21);
  finally
    Ini.Free;
  end;
end;

begin
  try
    ReadConfig;
    FTPClient := TFTPClient.Create(ServerHost, Port, 'admin', 'admin');
    FTPClient.Conect;
    try
      //Send
      if FindCmdLineSwitch('s', Lib) then
      begin
        FTPClient.Send(Lib);
        Writeln('Send: Lib = '+ Lib);
        Readln;
      end
      else
        HelpCmd;
      //Receive
      if FindCmdLineSwitch('r', Lib)  then
      begin
        if FindCmdLineSwitch('d', Dest)  then
        begin
          FTPClient.Download(Lib, Dest);
          Writeln('Receive: Lib = '+ Lib);
          Readln;
        end;
      end
      else
        HelpCmd;
    finally
      FTPClient.Free
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
