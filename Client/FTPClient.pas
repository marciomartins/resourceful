unit FTPClient;

interface

uses IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP,
  IdFTPCommon, System.Classes;

type
  TFTPClient = class
  private
    FTP: TIdFTP;
  public
    constructor Create(const Host: string; Port: Integer; const UserName, Password: string);
    destructor Destroy; override;
    procedure Conect;
    procedure Unconnect;
    function Connected: Boolean;
    function DateOfFile(const FileName: string): TDateTime;
    procedure Download(const FileName, Dest: string);
    procedure Send(const FileName: string);
    procedure Status(var AStatusList: TStrings);
  end;

implementation

{ TFTPClient }

function TFTPClient.Connected: Boolean;
begin
  Result := FTP.Connected;
end;

procedure TFTPClient.Conect;
begin
  FTP.Connect;
end;

procedure TFTPClient.Download(const FileName, Dest: string);
begin
  FTP.Get(FileName, Dest, False, FTP.ResumeSupported);
end;

procedure TFTPClient.Send(const FileName: string);
begin
  FTP.Put(FileName);
end;

procedure TFTPClient.Status(var AStatusList: TStrings);
begin
  FTP.LastCmdResult.Text.Text := AStatusList.Text;
  FTP.Status(AStatusList);
end;

constructor TFTPClient.Create(const Host: string; Port: Integer; const UserName, Password: string);
begin
  FTP := TIdFTP.Create;
  FTP.Host := Host;
  FTP.Port := Port;
  FTP.Username := UserName;
  FTP.Password := Password;
  FTP.ConnectTimeout := 10000;
end;

function TFTPClient.DateOfFile(const FileName: string): TDateTime;
begin
  Result := FTP.FileDate(FileName);
end;

procedure TFTPClient.Unconnect;
begin
  FTP.Disconnect;
end;

destructor TFTPClient.Destroy;
begin
  FTP.Free;
  inherited;
end;

end.
