unit FTPServer;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  IdBaseComponent, IdComponent, IdTCPServer, IdCmdTCPServer, IdFTPList,
  IdExplicitTLSClientServerBase, IdFTPServer, StdCtrls, IdFTPListOutput,
  IdCustomTCPServer, IdZLibCompressorBase, IdCompressorZLib, IdUserAccounts,
  IdCommandHandlers, FileUtils;

type
  TFTPServer = class
  private
    FTP: TIdFTPServer;
    IdCompressorZLib: TIdCompressorZLib;
    IdUserManager: TIdUserManager;
    procedure ConfigFTP;
    procedure FTPUserLogin(ASender: TIdFTPServerContext;
      const AUsername, APassword: string; var AAuthenticated: Boolean);
    procedure FTPRetrieveFile(ASender: TIdFTPServerContext;
      const AFileName: string; var VStream: TStream);
    procedure FTPStoreFile(ASender: TIdFTPServerContext;
      const AFileName: string; AAppend: Boolean; var VStream: TStream);
    procedure FTPFileExistCheck(ASender: TIdFTPServerContext;
      const APathName: string; var VExist: Boolean);
    procedure FTPGetFileDate(ASender: TIdFTPServerContext;
      const AFileName: string; var VFileDate: TDateTime);
    procedure FTPGetFileSize(ASender: TIdFTPServerContext;
      const AFileName: string; var VFileSize: Int64);
    function RemoveTrailingPathDel(const AData: String): String;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Start;
    procedure Stop;
    function Conectado: Boolean;
  end;

implementation

uses StrUtils, Settings;

{ TFTPServer }

function TFTPServer.RemoveTrailingPathDel(const AData: String): String;
begin
  Result := AData;
  repeat
    if Result = '' then
    begin
      Break;
    end;
    if (Result[Length(Result)] = '/') or (Result[Length(Result)] = '\') then
    begin
      System.Delete(Result, Length(Result), 1);
    end
    else
    if (Result[1] = '/') or (Result[1] = '\') then
    begin
      System.Delete(Result, 1, 1);
    end
    else
    begin
      Break;
    end;
  until False;
end;

constructor TFTPServer.Create;
var
  IdUserAccount: TIdUserAccount;
begin
  IdCompressorZLib := TIdCompressorZLib.Create(nil);
  FTP := TIdFTPServer.Create(nil);
  FTP.OnUserLogin := FTPUserLogin;
  FTP.OnRetrieveFile := FTPRetrieveFile;
  FTP.OnStoreFile := FTPStoreFile;
  FTP.OnFileExistCheck := FTPFileExistCheck;
  FTP.OnGetFileDate := FTPGetFileDate;
  FTP.OnGetFileSize := FTPGetFileSize;
  //
  FTP.Compressor := IdCompressorZLib;
  IdUserManager := TIdUserManager.Create(nil);
  IdUserAccount := IdUserManager.Accounts.Add;
  IdUserAccount.UserName := 'admin';
  IdUserAccount.Password := 'admin';
  FTP.UserAccounts := IdUserManager;
  ConfigFTP;
end;

destructor TFTPServer.Destroy;
begin
  IdCompressorZLib.Free;
  IdUserManager.Free;
  FTP.Free;
  inherited;
end;

function TFTPServer.Conectado: Boolean;
begin
  Result := FTP.Active;
end;

procedure TFTPServer.ConfigFTP;
begin
  FTP.AllowAnonymousLogin := False;
  FTP.AnonymousPassStrictCheck := False;
  FTP.AnonymousAccounts.Clear;
  FTP.ExceptionReply.Code := '500';
  FTP.MLSDFacts := FTP.MLSDFacts + [mlsdFileCreationTime,
    mlsdFileLastAccessTime, mlsdWin32Attributes];
  FTP.SystemType := 'WIN32';
  FTP.UseTLS := utNoTLSSupport;
  FTP.DefaultPort := LocalSettings.Port;
end;

procedure TFTPServer.FTPFileExistCheck(ASender: TIdFTPServerContext;
  const APathName: string; var VExist: Boolean);
begin
  VExist := FileExists(IncludeTrailingPathDelimiter
    (LocalSettings.ResorucePathRepository) + APathName);
end;

procedure TFTPServer.FTPGetFileDate(ASender: TIdFTPServerContext;
  const AFileName: string; var VFileDate: TDateTime);
begin
  VFileDate := GetFileDate(IncludeTrailingPathDelimiter
    (LocalSettings.ResorucePathRepository) + RemoveTrailingPathDel(AFileName));
end;

procedure TFTPServer.FTPGetFileSize(ASender: TIdFTPServerContext;
  const AFileName: string; var VFileSize: Int64);
Var
  LFile: String;
begin
  // we return -1 for an error
  LFile := IncludeTrailingPathDelimiter(LocalSettings.ResorucePathRepository) +
    AFileName;
  try
    If FileExists(LFile) then
      VFileSize := GetSizeOfFile(LFile)
    else
      VFileSize := -1;
  except
    VFileSize := -1;
  end;
end;

procedure TFTPServer.FTPRetrieveFile(ASender: TIdFTPServerContext;
  const AFileName: string; var VStream: TStream);
var
  LFile: string;
begin
  LFile := RemoveTrailingPathDel(AFileName);
  VStream := TFileStream.Create(IncludeTrailingPathDelimiter(LocalSettings.ResorucePathRepository) + LFile, fmOpenRead);
end;

procedure TFTPServer.FTPStoreFile(ASender: TIdFTPServerContext;
  const AFileName: string; AAppend: Boolean; var VStream: TStream);
begin
  if not AAppend then
  begin
    VStream := TFileStream.Create
      (IncludeTrailingPathDelimiter(LocalSettings.ResorucePathRepository) + RemoveTrailingPathDel(AFileName),
      fmCreate);
  end
  else
  begin
    VStream := TFileStream.Create
      (IncludeTrailingPathDelimiter(LocalSettings.ResorucePathRepository) + RemoveTrailingPathDel(AFileName),
      fmOpenWrite)
  end;
end;

procedure TFTPServer.FTPUserLogin(ASender: TIdFTPServerContext;
  const AUsername, APassword: string; var AAuthenticated: Boolean);
begin
  AAuthenticated := True;
end;

procedure TFTPServer.Start;
begin
  FTP.Active := True;
end;

procedure TFTPServer.Stop;
begin
  FTP.Active := False;
end;

end.
