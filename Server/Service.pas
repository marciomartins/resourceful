unit Service;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, FTPServer;

type
  TRService = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
  private
    { Private declarations }
    FTPServer: TFTPServer;
    procedure Start;
    procedure Stop;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  RService: TRService;

implementation

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  RService.Controller(CtrlCode);
end;

function TRService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TRService.Start;
begin
  FTPServer.Start;
end;

procedure TRService.Stop;
begin
  FTPServer.Stop;
end;

procedure TRService.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
  Start;
end;

procedure TRService.ServiceCreate(Sender: TObject);
begin
  FTPServer := TFTPServer.Create;
end;

procedure TRService.ServiceDestroy(Sender: TObject);
begin
  FTPServer.Free;
end;

procedure TRService.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Stop;
end;

procedure TRService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Start;
end;

procedure TRService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Stop;
end;

end.
