unit FileUtils;

interface

uses Windows, Forms, System.SysUtils, System.Classes;

type
  TCopyFileCallbackProc=procedure(const aBytesCopied:integer);
  TCopyFileCallbackEvent=procedure(const aBytesCopied:integer) of object;
  TCopyFileCallbackInfo=(ciFileSize,ciBytesCopied);
  TCopyFileCallbackProcEx=procedure(const aCount:integer; const aInfo:TCopyFileCallbackInfo);
  TCopyFileCallbackEventEx=procedure(const aCount:integer; const aInfo:TCopyFileCallbackInfo) of object;

function FileTimeToTDateTime(const AFileTime: TFileTime): TDateTime;
function TDateTimeToFileTime(const ADateTime: TDateTime): TFileTime;
function GetFileDate(AFile: String): TDateTime;
function GetSizeOfFile(AFile : String) : Integer;
function CopyFile(const aSource,aDest:string): Boolean; overload;
function CopyFile(const aSource,aDest:string; aCallbackProc:TCopyFileCallbackProc): Boolean; overload;
function CopyFile(const aSource,aDest:string; aCallbackEvent:TCopyFileCallbackEvent): Boolean; overload;
function CopyFile(const aSource,aDest:string; aCallbackProcEx:TCopyFileCallbackProcEx): Boolean; overload;
function CopyFile(const aSource,aDest:string; aCallbackEventEx:TCopyFileCallbackEventEx): Boolean; overload;
function FileVersion(const aFile:string=''):string;
function SetFileDateTime(FileName: string; CreateTime, ModifyTime, AcessTime: TDateTime): Boolean;
function GetWindowsDrive: Char;
function GetSerialHD: string;
procedure ListFiles(const Path: string; var List: TStringList);

implementation

var
  CopyFileMsg: string = '';

function FileTimeToTDateTime(const AFileTime: TFileTime): TDateTime;
var
  LDosTime: LongInt;
begin
  Result := 0;
  if Windows.FileTimeToDosDateTime(AFileTime, LongRec(LDosTime).Hi, LongRec(LDosTime).Lo) then
  begin
    Result := System.SysUtils.FileDateToDateTime(LDosTime);
  end
  else
  begin
    System.SysUtils.RaiseLastOSError;
  end;
end;

function TDateTimeToFileTime(const ADateTime: TDateTime): TFileTime;
var
  LDosTime: LongInt;
begin
  Result.dwLowDateTime := 0;
  Result.dwHighDateTime := 0;
  LDosTime := System.SysUtils.DateTimeToFileDate(ADateTime);
  DosDateTimeToFileTime(LongRec(LDosTime).Hi, LongRec(LDosTime).Lo, Result);
end;

function GetFileDate(AFile: String): TDateTime;
//You can't use the Borland Standard RTL FileAge function because
//that returns the time as the local time.  MDTM requires that the time
//be returned in GMT.
var
  LHandle : THandle;
  LModDate : TFileTime;
begin
  LHandle :=   Windows.CreateFile(PChar(AFile),GENERIC_READ, FILE_SHARE_READ OR FILE_SHARE_WRITE,nil,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);
  if LHandle = INVALID_HANDLE_VALUE then
  begin
    System.SysUtils.RaiseLastOSError;
  end;
  try
    //GetFileTime parameters that are nil.
    //Creation Time/ last access time
    Windows.GetFileTime(LHandle,nil,nil,@LModDate);
  finally
    Windows.CloseHandle(LHandle);
  end;
  Result := FileTimeToTDateTime(LModDate);
end;

function GetSizeOfFile(AFile : String) : Integer;
var
  FStream : TFileStream;
begin
  try
    FStream := TFileStream.Create(AFile, fmOpenRead);
    try
      Result := FStream.Size;
    finally
      FreeAndNil(FStream);
    end;
  except
    Result := 0;
  end;
end;

function __CopyFile(const aSource,aDest:string; aCallbackProc:TCopyFileCallbackProc=nil;
                                               aCallbackEvent:TCopyFileCallbackEvent=nil;
                                               aCallbackProcEx:TCopyFileCallbackProcEx=nil;
                                               aCallbackEventEx:TCopyFileCallbackEventEx=nil):boolean;
const
  buffsize=16384;
var
  qwanted,qread,qwritten:integer;
  fsource,fdest:file;
  buffer:pointer;
begin
  Result:=False;
  try
    CopyFileMsg:='';
    FileMode   :=fmOpenRead+fmShareDenyNone;
    AssignFile(fsource,aSource);
    Reset(fsource,1);
    try
      if assigned(aCallbackProcEx) then
        aCallbackProcEx(FileSize(fsource),ciFileSize);
      if assigned(aCallbackEventEx) then
        aCallbackEventEx(FileSize(fsource),ciFileSize);
      //
      AssignFile(fdest,aDest);
      Rewrite(fdest,1);
      try
        GetMem(buffer,buffsize);
        try
          qwanted:=buffsize;
          while not Eof(fsource) do
          begin
            BlockRead(fsource,buffer^,qwanted,qread);
            BlockWrite(fdest,buffer^,qread,qwritten);
            if qwritten<>qread then
              raise Exception.Create('Erro de grava��o desconhecido.');
            if assigned(aCallbackProc) then
              aCallbackProc(qwritten);
            if assigned(aCallbackEvent) then
              aCallbackEvent(qwritten);
            if assigned(aCallbackProcEx) then
              aCallbackProcEx(qwritten,ciBytesCopied);
            if assigned(aCallbackEventEx) then
              aCallbackEventEx(qwritten,ciBytesCopied);
          end;
        finally
          FreeMem(buffer);
          CloseFile(fdest);
        end;
      except
        DeleteFile(aDest);
        raise;
      end;
    finally
      CloseFile(fsource);
    end;
    Result:=True;
  except
    on e:Exception do
      CopyFileMsg := e.Message+' - c�pia de "'+aSource+'" para "'+aDest+'".';
  end;
end;

function CopyFile(const aSource,aDest:string): Boolean;
begin
  Result:=__CopyFile(aSource,aDest,nil,nil,nil,nil);
end;

function CopyFile(const aSource,aDest:string; aCallbackProc:TCopyFileCallbackProc): Boolean;
begin
  Result:=__CopyFile(aSource,aDest,aCallbackProc,nil,nil,nil);
end;

function CopyFile(const aSource,aDest:string; aCallbackEvent:TCopyFileCallbackEvent): Boolean;
begin
  Result:=__CopyFile(aSource,aDest,nil,aCallbackEvent,nil,nil);
end;

function CopyFile(const aSource,aDest:string; aCallbackProcEx:TCopyFileCallbackProcEx): Boolean;
begin
  Result:=__CopyFile(aSource,aDest,nil,nil,aCallbackProcEx,nil);
end;

function CopyFile(const aSource,aDest:string; aCallbackEventEx:TCopyFileCallbackEventEx): Boolean;
begin
  Result:=__CopyFile(aSource,aDest,nil,nil,nil,aCallbackEventEx);
end;

function FileVersion(const aFile:string=''):string;
var
  h,q:DWORD;
  i:pointer;
  f:PVSFixedFileInfo;
  l:UInt;
  n:string;
begin
  Result:='';
  if aFile<>'' then
    n:=aFile
  else
    n := Application.ExeName;
  q:=GetFileVersionInfoSize(PChar(n),h);
  if q=0 then
    Exit;
  getmem(i,q);
  try
    if GetFileVersionInfo(PChar(n),h,q,i) then
      if VerQueryValue(i,'\',pointer(f),l) then
        Result:=IntToStr(HIWORD(f^.dwFileVersionMS))+'.'+IntToStr(LOWORD(f^.dwFileVersionMS))+'.'+IntToStr(HIWORD(f^.dwFileVersionLS))+'.'+ IntToStr(LOWORD(f^.dwFileVersionLS));
  finally
    freemem(i,q);
  end;
end;

function SetFileDateTime(FileName: string; CreateTime, ModifyTime, AcessTime: TDateTime): Boolean;
  function ConvertToFileTime(DateTime :TDateTime) :PFileTime;
  var
    FileTime :TFileTime;
    LFT: TFileTime;
    LST: TSystemTime;
  begin
    Result := nil;
    if DateTime > 0 then
    begin
      DecodeDate(DateTime, LST.wYear, LST.wMonth, LST.wDay);
      DecodeTime(DateTime, LST.wHour, LST.wMinute, LST.wSecond, LST.wMilliSeconds);
      if SystemTimeToFileTime(LST, LFT) then
        if LocalFileTimeToFileTime(LFT, FileTime) then
        begin
          New(Result);
          Result^ := FileTime;
        end;
    end;
  end;
var
  FileHandle: Integer;
  ftCreateTime,
  ftModifyTime,
  ftAcessTime: PFileTime;
begin
  ftCreateTime := ConvertToFileTime(CreateTime);
  ftModifyTime := ConvertToFileTime(ModifyTime);
  ftAcessTime  := ConvertToFileTime(AcessTime);
  try
    FileHandle := FileOpen(FileName, fmOpenReadWrite or fmShareExclusive);
    try
      Result := SetFileTime(FileHandle, ftCreateTime, ftAcessTime, ftModifyTime);
    finally
      FileClose(FileHandle);
    end;
  finally
    Dispose(ftCreateTime);
    Dispose(ftAcessTime);
    Dispose(ftModifyTime);
  end;
end;

function GetWindowsDrive: Char;
var
  S: string;
begin
  SetLength(S, MAX_PATH);
  if GetWindowsDirectory(PChar(S), MAX_PATH) > 0 then
    Result := string(S)[1]
  else
    Result := #0;
end;

function GetSerialHD: string;
Var
  Serial:DWord;
  DirLen,Flags: DWord;
  DLabel : Array[0..11] of Char;
begin
  Try
    GetVolumeInformation(PChar(GetWindowsDrive+':\'),dLabel,12,@Serial,DirLen,Flags,nil,0);
    Result := IntToHex(Serial,8);
  Except
    Result :='';
  end;
end;

procedure ListFiles(const Path: string; var List: TStringList);
var
  F: TSearchRec;
  Ret: Integer;
begin
  Ret := FindFirst(Path+'\*.*', faAnyFile, F);
  try
    while Ret = 0 do
    begin
      List.Add(F.Name);
      Ret := FindNext(F);
    end;
  finally
    begin
      System.SysUtils.FindClose(F);
    end;
  end;
end;

end.
