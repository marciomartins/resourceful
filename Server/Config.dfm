object frmConfig: TfrmConfig
  Left = 0
  Top = 0
  Caption = 'Resourceful [Configuration]'
  ClientHeight = 208
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    425
    208)
  PixelsPerInch = 96
  TextHeight = 13
  object ledPath: TLabeledEdit
    Left = 8
    Top = 24
    Width = 409
    Height = 21
    EditLabel.Width = 129
    EditLabel.Height = 13
    EditLabel.Caption = 'Resource Repository Path:'
    TabOrder = 0
    TextHint = 'C:\Resourcefull\repository'
  end
  object BitBtn1: TBitBtn
    Left = 342
    Top = 175
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 261
    Top = 175
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Ok'
    TabOrder = 2
    OnClick = BitBtn2Click
  end
  object ledPort: TLabeledEdit
    Left = 8
    Top = 64
    Width = 49
    Height = 21
    EditLabel.Width = 24
    EditLabel.Height = 13
    EditLabel.Caption = 'Port:'
    TabOrder = 3
    TextHint = '21'
  end
  object BitBtn3: TBitBtn
    Left = 181
    Top = 62
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Start'
    TabOrder = 4
    OnClick = BitBtn3Click
  end
  object BitBtn4: TBitBtn
    Left = 262
    Top = 62
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Stop'
    TabOrder = 5
    OnClick = BitBtn4Click
  end
end
