program rs;

uses
  Vcl.Forms,
  Vcl.SvcMgr,
  System.SysUtils,
  Config in 'Config.pas' {frmConfig},
  Service in 'Service.pas' {RService: TService},
  FTPServer in 'FTPServer.pas',
  Settings in 'Settings.pas',
  FileUtils in 'FileUtils.pas',
  StrUtils in 'StrUtils.pas';

{$R *.res}

begin
  if FindCmdLineSwitch('config') then
  begin
    Vcl.Forms.Application.Initialize;
    Vcl.Forms.Application.Title := 'Resourceful Services';
    Vcl.Forms.Application.CreateForm(TfrmConfig, frmConfig);
    Vcl.Forms.Application.Run;
  end
  else
  begin
    try
      if not Vcl.SvcMgr.Application.DelayInitialize or Vcl.SvcMgr.Application.Installing then
        Vcl.SvcMgr.Application.Initialize;
      Vcl.SvcMgr.Application.CreateForm(TRService, RService);
      Vcl.SvcMgr.Application.Run;
    except
      on E: Exception do
      begin
        raise Exception.Create(e.Message);
      end;
    end;
  end;
end.
