unit StrUtils;

interface

function SubString(Str: string; Parte: integer; Separador: string): string;

implementation

function SubString(Str: string; Parte: integer; Separador: string): string;
var
  lp0, sln, CurrN: integer;
  EmSep, PrimEncontrado: boolean;
begin
  result := '';
  CurrN := 0;
  EmSep := false;
  PrimEncontrado := false;
  sln := length(Str);
  for lp0 := 1 to sln do
  begin
    if Pos(Str[lp0],Separador) > 0 then
    begin
      if not EmSep then
      begin
        if PrimEncontrado then
          inc(CurrN);
      end;
      EmSep := true;
      if CurrN > Parte then
        break;
     end
     else
     begin
       EmSep := false;
       PrimEncontrado := true;
       if CurrN = Parte then
         result := result + Str[lp0];
    end;
  end;
end;

end.
