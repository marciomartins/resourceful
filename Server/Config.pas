unit Config;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, FTPServer;

type
  TfrmConfig = class(TForm)
    ledPath: TLabeledEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ledPort: TLabeledEdit;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    FTPServer: TFTPServer;
  public
    { Public declarations }
  end;

var
  frmConfig: TfrmConfig;

implementation

{$R *.dfm}

uses Settings;

procedure TfrmConfig.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmConfig.BitBtn2Click(Sender: TObject);
begin
  LocalSettings.ResorucePathRepository := ledPath.Text;
  LocalSettings.Port := StrToInt(ledPort.Text);
  LocalSettings.SaveConfig;
  //todo: Needed implement restart service
  Close;
end;

procedure TfrmConfig.BitBtn3Click(Sender: TObject);
begin
  FTPServer.Start;
end;

procedure TfrmConfig.BitBtn4Click(Sender: TObject);
begin
  FTPServer.Stop;
end;

procedure TfrmConfig.FormCreate(Sender: TObject);
begin
  FTPServer := TFTPServer.Create;
  LocalSettings.ReadConfig;
  ledPath.Text := LocalSettings.ResorucePathRepository;
  ledPort.Text := IntToStr(LocalSettings.Port);
end;

procedure TfrmConfig.FormDestroy(Sender: TObject);
begin
  FTPServer.Free;
end;

end.
